package com.cafetiere.content.service;

import com.cafetiere.content.model.Comment;
import com.cafetiere.content.model.Post;
import com.cafetiere.content.repository.CommentRepository;
import com.cafetiere.content.repository.PostRepository;
import com.cafetiere.content.vo.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class CommentServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private PostRepository postRepository;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private PostService postService;

    @InjectMocks
    private CommentServiceImpl commentService;

    private Post post;
    private Comment comment, comment2;
    private Account account, account2;

    @BeforeEach
    public void setUp(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        post = new Post("TestTitle",
                "TestContent",
                publishedTime,
                "link.jpg");

        calendar.set(2020, 1, 1, 1, 0, 0);
        Date publishedTime2 = calendar.getTime();
        comment = new Comment("TestIsiComment", publishedTime2);

        account = new Account("TestNamaDepan",
                "TestNamaBelakang",
                "TestEmail@mail.com",
                "TestUsername",
                "TestPassword",
                0);
        account.setId(0);

        account2 = new Account("TestNamaDepan2",
                "TestNamaBelakang2",
                "TestEmail2@mail.com",
                "TestUsername2",
                "TestPassword2",
                1);
        account2.setId(1);

        comment2 = new Comment("TestIsiComment2", publishedTime2);
        comment2.setIdComment(1);
    }

    @Test
    void testServiceCreateComment() {
        when(postService.getPostByIdPost(post.getIdPost())).thenReturn(post);
        when(commentRepository.save(comment)).thenReturn(comment);
        Comment newComment = commentService
                .createComment(comment, account.getId(), post.getIdPost());
        assertEquals(comment.getPost().getIdPost(), post.getIdPost());
        assertEquals(comment.getIdAccount(), account.getId());
        assertEquals(comment.getIsiComment(), newComment.getIsiComment());
        verify(commentRepository).save(comment);
    }

    @Test
    void testServiceGetCommentByCommentId(){
        lenient().when(commentService.getCommentByCommentId(comment.getIdComment())).thenReturn(comment);
        assertEquals(comment, commentService.getCommentByCommentId(comment.getIdComment()));
    }

    @Test
    void testServiceGetCommentByPostId(){
        List<Comment> listOfComment = commentRepository.findCommentsByPostIdPostOrderByIdCommentDesc(post.getIdPost());
        lenient().when(commentService.getCommentByPostId(post.getIdPost())).thenReturn(listOfComment);
        List<Comment> listOfCommentResult = commentService.getCommentByPostId(post.getIdPost());
        assertEquals(listOfComment, listOfCommentResult);
    }

    @Test
    void testServiceDeleteCommentByAdmin() {
        when(restTemplate.getForObject("http://AUTH-SERVICE/user/" + 0, Account.class))
                .thenReturn(account);
        postService.createPost(post, account.getId());
        commentService.createComment(comment, account.getId(), post.getIdPost());
        when(commentRepository.findCommentByIdComment(0)).thenReturn(comment);
        commentService.deleteCommentById(account.getId(), comment.getIdComment());
        lenient().when(commentService.getCommentByCommentId(comment.getIdComment()))
                .thenReturn(null);
        assertEquals(null, commentService.getCommentByCommentId(comment.getIdComment()));
        verify(commentRepository).deleteById(comment.getIdComment());
    }

    @Test
    void testServiceDeleteCommentByBuyer() {
        when(restTemplate.getForObject("http://AUTH-SERVICE/user/" + 1, Account.class))
                .thenReturn(account2);
        postService.createPost(post, account.getId());
        commentService.createComment(comment2, account2.getId(), post.getIdPost());
        when(commentRepository.findCommentByIdComment(1)).thenReturn(comment2);
        commentService.deleteCommentById(account2.getId(), comment2.getIdComment());
        lenient().when(commentService.getCommentByCommentId(comment2.getIdComment()))
                .thenReturn(null);
        assertEquals(null, commentService.getCommentByCommentId(comment2.getIdComment()));
        verify(commentRepository).deleteById(comment2.getIdComment());
    }
}
