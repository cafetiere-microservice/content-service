package com.cafetiere.content.service;

import com.cafetiere.content.model.Comment;
import com.cafetiere.content.model.Post;
import com.cafetiere.content.repository.PostRepository;
import com.cafetiere.content.vo.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class PostServiceTest {

    @Mock
    private PostRepository postRepository;

    @InjectMocks
    private PostServiceImpl postService;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private CommentService commentService;


    private Post post;
    private Comment comment;
    private Account account, account2;

    @BeforeEach
    public void setUp(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date publishedTime = calendar.getTime();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        post = new Post("TestTitle", "TestContent", publishedTime, "link.jpg");

        calendar.set(2020, 1, 1, 1, 0, 0);
        Date publishedTime2 = calendar.getTime();
        comment = new Comment("TestIsiComment", publishedTime2);

        account = new Account("TestNamaDepan",
                "TestNamaBelakang",
                "TestEmail@mail.com",
                "TestUsername",
                "TestPassword",
                0);
        account.setId(0);

        post.setAccountId(0);
        comment.setPost(post);

        account2 = new Account("TestNamaDepan2",
                "TestNamaBelakang2",
                "TestEmail2@mail.com",
                "TestUsername2",
                "TestPassword2",
                1);
        account2.setId(1);

        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        post.setListOfComment(commentList);

    }

    @Test
    void testServiceCreatePost() {
        when(restTemplate.getForObject("http://AUTH-SERVICE/user/" + 0, Account.class))
            .thenReturn(account);
        when(postRepository.save(post)).thenReturn(post);
        Post newPost = postService.createPost(post, account.getId());
        assertEquals(post.getTitlePost(), newPost.getTitlePost());
        verify(postRepository).save(post);
    }

    @Test
    void testServiceCreatePostShouldReturnNull() {
        when(restTemplate.getForObject("http://AUTH-SERVICE/user/" + 1, Account.class))
                .thenReturn(account2);
        assertEquals(null, postService.createPost(post, account2.getId()));
    }

    @Test
    void testServiceGetPostByIdPost(){
        lenient().when(postService.getPostByIdPost(post.getIdPost())).thenReturn(post);
        assertEquals(post, postService.getPostByIdPost(post.getIdPost()));
    }

    @Test
    void testServiceUpdatePost() {
        when(restTemplate.getForObject("http://AUTH-SERVICE/user/" + 0, Account.class))
                .thenReturn(account);
        postService.createPost(post, account.getId());
        when(postRepository.findPostsByIdPost(0))
                .thenReturn(post);
        String currentContent = post.getContentPost();
        post.setContentPost("PROMO!");
        lenient().when(postService.updatePostById(post, post.getIdPost(), account.getId()))
                .thenReturn(post);
        Post resultPost = postService.getPostByIdPost(post.getIdPost());
        assertNotEquals(currentContent, resultPost.getContentPost());
        assertEquals(post.getContentPost(), resultPost.getContentPost());
    }

    @Test
    void testServiceUpdatePostShouldReturnNull() {
        when(restTemplate.getForObject("http://AUTH-SERVICE/user/" + 0, Account.class))
                .thenReturn(account);
        postService.createPost(post, account.getId());
        post.setAccountId(account.getId());
        when(postRepository.findPostsByIdPost(post.getIdPost())).thenReturn(post);
        lenient().when(postService.updatePostById(post, post.getIdPost(), account2.getId()))
                .thenReturn(post);
        assertNotEquals(post.getAccountId(), account2.getId());
        assertEquals(null, postService.updatePostById(post, post.getIdPost(), account2.getId()));
    }

    @Test
    void testServiceDeletePostByIdPost() {
        when(restTemplate.getForObject("http://AUTH-SERVICE/user/" + 0, Account.class))
                .thenReturn(account);
        postService.createPost(post, account.getId());
        when(postRepository.findPostsByIdPost(0)).thenReturn(post);
        postService.deletePostById(post.getIdPost(), account.getId());
        lenient().when(postService.getPostByIdPost(post.getIdPost())).thenReturn(null);
        assertEquals(null, postService.getPostByIdPost(post.getIdPost()));
        verify(postRepository).deleteById(post.getIdPost());
    }

    @Test
    void testGetAllPosts(){
        Iterable<Post> listPost = postRepository.findAllByUpdatedTimeBeforeOrderByUpdatedTimeDesc(new Date());
        lenient().when(postService.getAllPosts()).thenReturn(listPost);
        Iterable<Post> listPostResult = postService.getAllPosts();
        Assertions.assertIterableEquals(listPost, listPostResult);
    }

    @Test
    void testAllPostsContainsKeyword(){
        Iterable<Post> listPost = postRepository.findByTitlePostContainingIgnoreCase("Title");
        lenient().when(postService.getAllPostsContainsKeyword("Test")).thenReturn(listPost);
        Iterable<Post> listPostResult = postService.getAllPostsContainsKeyword("Title");
        Assertions.assertIterableEquals(listPost, listPostResult);
    }



}
