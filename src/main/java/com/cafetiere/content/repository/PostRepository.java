package com.cafetiere.content.repository;

import com.cafetiere.content.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {
    Post findPostsByIdPost(Integer idPost);

    List<Post> findByTitlePostContainingIgnoreCase(String keyword);

    List<Post> findAllByUpdatedTimeBeforeOrderByUpdatedTimeDesc(Date time);
}
