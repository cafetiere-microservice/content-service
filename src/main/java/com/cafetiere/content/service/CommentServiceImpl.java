package com.cafetiere.content.service;

import com.cafetiere.content.model.Comment;
import com.cafetiere.content.model.Post;
import com.cafetiere.content.repository.CommentRepository;
import com.cafetiere.content.vo.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostService postService;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Comment createComment(Comment comment, int idAccount, int idPost) {
        Post post = postService.getPostByIdPost(idPost);
        comment.setPost(post);
        comment.setIdAccount(idAccount);
        commentRepository.save(comment);
        return comment;
    }

    @Override
    public Comment getCommentByCommentId(int idComment) {
        return commentRepository.findCommentByIdComment(idComment);
    }

    @Override
    public List<Comment> getCommentByPostId(int idPost) {
        return commentRepository.findCommentsByPostIdPostOrderByIdCommentDesc(idPost);
    }

    @Override
    public void deleteCommentById(int idAccount, int idComment) {
        Comment comment = commentRepository.findCommentByIdComment(idComment);
        Account account = restTemplate.getForObject("http://AUTH-SERVICE/user/" + comment.getIdAccount(), Account.class);
        if (account.getRole() == 0) {
            commentRepository.deleteById(idComment);
        } else if (account.getRole() == 1 && comment.getIdAccount() == idAccount) {
            commentRepository.deleteById(idComment);
        }
    }
}
