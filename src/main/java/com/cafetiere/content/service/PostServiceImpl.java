package com.cafetiere.content.service;

import com.cafetiere.content.model.Comment;
import com.cafetiere.content.model.Post;
import com.cafetiere.content.repository.PostRepository;
import com.cafetiere.content.vo.Account;
import com.cafetiere.content.vo.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentService commentService;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Post createPost(Post post, int idAccount) {
        Account account = restTemplate.getForObject("http://AUTH-SERVICE/user/" + idAccount, Account.class);
        if (account.getRole() == 0) {
            post.setAccountId(idAccount);
            postRepository.save(post);
            Notification notification = new Notification();
            notification.setTitle("Post Baru");
            notification.setBody("Silahkan cek home website untuk membaca postingan terbaru dari kita.");
            HttpEntity<Notification> entity = new HttpEntity<Notification>(notification);
            restTemplate.exchange("http://NOTIFICATION-SERVICE/notification/", HttpMethod.POST, entity, String.class);
            return post;
        }
        return null;
    }

    @Override
    public Post getPostByIdPost(Integer idPost) {
        return postRepository.findPostsByIdPost(idPost);
    }

    @Override
    public Iterable<Post> getAllPostsContainsKeyword(String keyword) {
        return postRepository.findByTitlePostContainingIgnoreCase(keyword);
    }

    @Override
    public Iterable<Post> getAllPosts() {
        return postRepository.findAllByUpdatedTimeBeforeOrderByUpdatedTimeDesc(new Date());
    }

    @Override
    public Post updatePostById(Post post, int idPost, int idAccount) {
        Post currentPost = postRepository.findPostsByIdPost(idPost);
        if (currentPost.getAccountId() == idAccount) {
            post.setIdPost(idPost);
            int idAdmin = currentPost.getAccountId();
            post.setAccountId(idAdmin);
            List<Comment> listOfComment = currentPost.getListOfComment();
            post.setUpdatedTime(new Date());
            post.setListOfComment(listOfComment);
            postRepository.save(post);
            return post;
        }
        return null;
    }

    @Override
    public void deletePostById(int idPost, int idAccount) {
        Post post = postRepository.findPostsByIdPost(idPost);
        Account account = restTemplate.getForObject("http://AUTH-SERVICE/user/" + post.getAccountId(), Account.class);
        if (account.getRole() == 0 && post.getAccountId() == idAccount) {
            for (Comment comment : post.getListOfComment()) {
                commentService.deleteCommentById(idAccount, comment.getIdComment());
            }
            postRepository.deleteById(idPost);
        }
    }


}
