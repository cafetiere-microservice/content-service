package com.cafetiere.content.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "post")
@Data
@NoArgsConstructor
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_post", updatable = false, nullable = false)
    private int idPost;

    @Column(name = "title_post")
    private String titlePost;

    @Column(name = "content_post")
    private String contentPost;

    @Column(name = "image_link")
    private String imageLink;

    @Column(name = "`timestamp`")
    @CreationTimestamp
    private Date publishedTime;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedTime;

    @JoinColumn(name = "account_id", nullable = false)
    private int accountId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
    private List<Comment> listOfComment;

    /**
     * Constructor Post.
     */
    public Post(String titlePost, String contentPost, Date publishedTime, String imageLink) {
        this.titlePost = titlePost;
        this.contentPost = contentPost;
        this.publishedTime = publishedTime;
        this.imageLink = imageLink;
    }
}