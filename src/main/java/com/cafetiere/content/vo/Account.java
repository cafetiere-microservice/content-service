package com.cafetiere.content.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private int role;

    /**
     * Constructor Account.
     */
    public Account(String firstName,
                   String lastName,
                   String email,
                   String username,
                   String password,
                   Integer role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
    }
}
