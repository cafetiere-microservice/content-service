package com.cafetiere.content.vo;

import com.cafetiere.content.model.Post;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplate {
    private Account account;
    private Post post;
}
